import { client } from "./app";
import chalk from "chalk";
import { readdir } from "fs";
import AbstractCommand from "./commands/AbstractCommand";
import onMessage from "./listeners/MessageListener";

export class DiscordBot {

    private token: string;
    static commands: Map<String, AbstractCommand> = new Map();

    public constructor(token: string) {

        this.token = token;

    }

    public async start() {

        await client.login(this.token);
        console.info(`${chalk.green('[INFO]')} MineNationXL bot is now listening for events`);

        client.user.setPresence({
            game: {
                name: 'met de plugin',
                type: 'PLAYING'
            },
            status: 'dnd'
        });

        // Load all commands
        await this.loadCommands();

        // Register listeners
        client.on("message", onMessage);

        console.info(`${chalk.green('[INFO]')} MineNationXL bot is now running.`);

    }

    private loadCommands() {

        readdir(__dirname + "/commands", async (err: NodeJS.ErrnoException, filenames: Array<string>) => {

            if (err) {
                console.error(err.message);
                return;
            }

            filenames.forEach((name: string, index: number, names: Array<string>) => {

                if (name.endsWith(".map") || name == "AbstractCommand.js") return;

                let command: AbstractCommand = new (require(`./commands/${name}`).default)();
                DiscordBot.commands.set(command.name, command);

                console.info(`${chalk.green("[INFO]")} Command '!${command.name}' registered.`);

            });

            console.info(`${chalk.green("[INFO]")} All commands registered.`);

        });

    }

}