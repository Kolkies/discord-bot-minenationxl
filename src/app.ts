import * as Discord from 'discord.js';
import * as config_file from '../config.json';
import { Main } from './Main.js';

export const config = (<any>config_file);
export const client = new Discord.Client(); 

new Main(config.bot_token).boot();